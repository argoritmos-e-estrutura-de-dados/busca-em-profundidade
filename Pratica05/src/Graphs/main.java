/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Graphs;

/**
 *
 * @author messiah
 */
public class main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Grafo 1:");
        JGraph jg_0 = new JGraph(8);
        jg_0.insertEdge(0, 1, 1);
        jg_0.insertEdge(0, 3, 1);
        jg_0.insertEdge(0, 4, 1);
        jg_0.insertEdge(1, 0, 1);
        jg_0.insertEdge(1, 2, 1);
        jg_0.insertEdge(1, 5, 1);
        jg_0.insertEdge(2, 1, 1);
        jg_0.insertEdge(2, 3, 1);
        jg_0.insertEdge(2, 6, 1);
        jg_0.insertEdge(3, 0, 1);
        jg_0.insertEdge(3, 2, 1);
        jg_0.insertEdge(3, 7, 1);
        jg_0.insertEdge(4, 0, 1);
        jg_0.insertEdge(4, 5, 1);
        jg_0.insertEdge(4, 7, 1);
        jg_0.insertEdge(5, 1, 1);
        jg_0.insertEdge(5, 4, 1);
        jg_0.insertEdge(5, 6, 1);
        jg_0.insertEdge(6, 2, 1);
        jg_0.insertEdge(6, 5, 1);
        jg_0.insertEdge(6, 7, 1);
        jg_0.insertEdge(7, 3, 1);
        jg_0.insertEdge(7, 4, 1);
        jg_0.insertEdge(7, 6, 1);
        jg_0.print();
           
        JCicle jc_0 = new JCicle(8, jg_0);
        System.out.println("O grafo "+(jc_0.search(0)?"é ":"não é ")+"cíclico.\n");
                       
        System.out.println("Grafo 2: ");        
        JGraph jg_1 = new JGraph(10);
        jg_1.insertEdge(0, 1, 1);
        jg_1.insertEdge(0, 2, 1);
        jg_1.insertEdge(0, 3, 1);
        jg_1.insertEdge(0, 5, 1);
        jg_1.insertEdge(1, 2, 1);
        jg_1.insertEdge(2, 3, 1);
        jg_1.insertEdge(2, 4, 1);
        jg_1.insertEdge(4, 6, 1);
        jg_1.insertEdge(5, 4, 1);
        jg_1.insertEdge(5, 6, 1);
        jg_1.insertEdge(6, 7, 1);
        jg_1.insertEdge(6, 8, 1);
        jg_1.insertEdge(7, 8, 1);
        jg_1.insertEdge(9, 6, 1);
        jg_1.print();
        
        JCicle jc_1 = new JCicle(10, jg_1);
        System.out.println("O grafo "+(jc_1.search(0)?"é ":"não é ")+"cíclico.\n");
    }

}
