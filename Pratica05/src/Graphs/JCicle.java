package Graphs;

import java.util.Stack;

/** @author messiah */

/** A busca em profundidade pode ser usada para verificar se um grafo é acíclico
ou contém um ou mais ciclos; Se uma aresta de retorno é encontrada durante a busca
em profundidade em G, então o grafo tem ciclo; Um grafo direcionado G é acíclico
se e somente se a busca em profundidade em G não apresentar arestas de retorno. */

public class JCicle {
    private boolean already_visited[];
    private Stack<Boolean> st;
    private final JGraph jg;
    
    public JCicle(int n, JGraph jg){
        this.already_visited = new boolean[n];
        for(int i=0; i<n; i++) this.already_visited[i] = false;
        this.st = new Stack<>();
        this.jg = jg;
    }
    public boolean search(int n){
        this.already_visited[n] = true;
        for(int i=0; i<this.jg.getN(); i++){
            if(this.jg.getE()[n][i] != 0){
                if(this.already_visited[i] == true) 
                        return false;
                this.search(i);
            }
        }
        return true;
    }
}